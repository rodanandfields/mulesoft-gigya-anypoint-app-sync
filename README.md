# Utility to propagate Client Application credentials from Gigya to Anypoint

RodanandFields uses Gigya for Client Management. 
However, it is a known issue that Gigya does not support Dynamic Client Registration for Oauth flows. 
Gigya also does not expose APIs to propagate the client credentials created in the Anypoint Platform to Gigya.
This presents a challenge on how does one go about propagating the client credentials from Gigya to Anypoint. Until Gigya provides native DCR support, a short term solution is to leverage Anypoint Platform APIs to create the client application in Anypoint by passing the client credentials created in Gigya.

  
This Mule application does the following:
- Creates Client Application in Anypoint using the credentials provided by the user
- Sets the users associated with the emailIds (provided in the request) as owners of the client application (to allow them to manage the application going forward)


## URL

POST call to `http://{host}:port/api/applications`

## config.yaml

The yaml file needs to be updated with the customer's Anypoint Platform Org Id and service account credentials.


## Sample Request

```
{
	"clientName":"test",
	"description": "Test app",
	"clientId": "2134",
	"clientSecret": "qe2t",
	"appOwnerEmailIds":["xyz@domain.com"],
	"redirectUri": ["http://test.com","https://test.com"],
	"appUrl": "https://test.com"
	
}

```


## Sample Response

```
{
  "appId": 254682,
  "clientName": "test",
  "clientId": "2134"
}
```

## Contributors

Nisha Nizamuddin